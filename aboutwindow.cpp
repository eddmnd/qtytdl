#include "aboutwindow.h"
#include "ui_aboutwindow.h"
#include "aboutytdlwindow.h"

aboutwindow::aboutwindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::aboutwindow)
{
    ui->setupUi(this);
}

aboutwindow::~aboutwindow()
{
    delete ui;
}

void aboutwindow::on_pushButton_clicked()
{
    aboutytdlwindow abtytdl;
    abtytdl.setModal(true);
    abtytdl.exec();
}
