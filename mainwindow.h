#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionHelp_triggered();

    void on_actionAbout_2_triggered();

    void on_pushButton_DL_clicked();

    void on_lineEdit_link_cursorPositionChanged(int arg1, int arg2);

    void on_checkBox_pl_stateChanged(int arg1);

    void on_checkBox_ao_stateChanged(int arg1);

    void on_checkBox_stateChanged(int arg1);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
