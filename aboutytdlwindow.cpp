#include "aboutytdlwindow.h"
#include "ui_aboutytdlwindow.h"

aboutytdlwindow::aboutytdlwindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::aboutytdlwindow)
{
    ui->setupUi(this);
}

aboutytdlwindow::~aboutytdlwindow()
{
    delete ui;
}

void aboutytdlwindow::on_pushButton_clicked()
{
    aboutytdlwindow::close();
}
