#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "helpwindow.h"
#include "aboutwindow.h"

#include "QProcess"
#include "QDir"
#include "QDebug"
#include "QMessageBox"

QString linkk,x_arg="-f mp4",pl_arg="--no-playlist",home=QDir::homePath();

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->checkBox->setVisible(false);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionHelp_triggered()
{
    helpwindow hlp;
    hlp.setModal(true);
    hlp.exec();
}

void MainWindow::on_actionAbout_2_triggered()
{
    aboutwindow abt;
    abt.setModal(true);
    abt.exec();
}

void MainWindow::on_lineEdit_link_cursorPositionChanged(int arg1, int arg2)
{
    linkk=ui->lineEdit_link->text();
    qDebug()<<linkk;
}

void MainWindow::on_checkBox_pl_stateChanged(int arg1)
{
    if(arg1==0){QProcess::execute("echo play list");pl_arg="";}
    else{qDebug()<<"no playlist";pl_arg="--no-playlist";}
}

void MainWindow::on_checkBox_ao_stateChanged(int arg1)
{
    if(arg1==0){QProcess::execute("echo video");x_arg="-f mp4";ui->checkBox->setVisible(false);}
    else{qDebug()<<"audio";x_arg="-x";ui->checkBox->setVisible(true);}
}

void MainWindow::on_checkBox_stateChanged(int arg1)
{
    if(arg1==0){QProcess::execute("echo not mp3");x_arg="-x";}
    else{qDebug()<<"mp3";x_arg="-x --audio-format mp3";}
}

void MainWindow::on_pushButton_DL_clicked()
{
    QProcess *process = new QProcess();
    QString a="youtube-dl "+x_arg+" "+pl_arg+" -f best"
                                             " -o\""+home+"/Downloads/%(title)s.%(ext)s\" "+linkk;
    if(!linkk.contains("https://")){QMessageBox::information(this,"Error","Enter a Valid Link.");}
    else{process->execute(a);QMessageBox::information
                (this,"CorYoutubeDownloader","Check Downloads Folder.");}
}
