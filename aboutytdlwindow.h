#ifndef ABOUTYTDLWINDOW_H
#define ABOUTYTDLWINDOW_H

#include <QDialog>

namespace Ui {
class aboutytdlwindow;
}

class aboutytdlwindow : public QDialog
{
    Q_OBJECT

public:
    explicit aboutytdlwindow(QWidget *parent = nullptr);
    ~aboutytdlwindow();

private slots:
    void on_pushButton_clicked();

private:
    Ui::aboutytdlwindow *ui;
};

#endif // ABOUTYTDLWINDOW_H
